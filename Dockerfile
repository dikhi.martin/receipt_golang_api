FROM scratch
ENV TZ=Asia/Jakarta
COPY app/app.run /bin/server
COPY app/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY app/nsswitch.conf /etc/nsswitch.conf
COPY app/logs /bin/logs
WORKDIR /bin
CMD ["server"]