# Receipt API Golang | GORM | v.1.0.0
<a href="https://echo.labstack.com"><img height="80" src="https://cdn.labstack.com/images/echo-logo.svg"></a>

[![Sourcegraph](https://sourcegraph.com/github.com/labstack/echo/-/badge.svg?style=flat-square)](https://sourcegraph.com/github.com/labstack/echo?badge)
[![GoDoc](http://img.shields.io/badge/go-documentation-blue.svg?style=flat-square)](http://godoc.org/github.com/labstack/echo)
[![Go Report Card](https://goreportcard.com/badge/github.com/labstack/echo?style=flat-square)](https://goreportcard.com/report/github.com/labstack/echo)
[![Build Status](http://img.shields.io/travis/labstack/echo.svg?style=flat-square)](https://travis-ci.org/labstack/echo)
[![Codecov](https://img.shields.io/codecov/c/github/labstack/echo.svg?style=flat-square)](https://codecov.io/gh/labstack/echo)
[![Join the chat at https://gitter.im/labstack/echo](https://img.shields.io/badge/gitter-join%20chat-brightgreen.svg?style=flat-square)](https://gitter.im/labstack/echo)
[![Forum](https://img.shields.io/badge/community-forum-00afd1.svg?style=flat-square)](https://forum.labstack.com)
[![Twitter](https://img.shields.io/badge/twitter-@labstack-55acee.svg?style=flat-square)](https://twitter.com/labstack)
[![License](http://img.shields.io/badge/license-mit-blue.svg?style=flat-square)](https://raw.githubusercontent.com/labstack/echo/master/LICENSE)

## Base URL
|  |  |
| ------ | ------ |
<!-- | URL Production | https://poskita.monstercode.net/api/v1/online | -->
<!-- | URL Development  | http://192.168.88.34:4000/api/v1/online  | -->

## API Documentation
<!-- https://documenter.getpostman.com/view/6477946/TzkzpJyU -->

## Cara Menjalankan
``` shell
cp app/.env.example app/.env
cd app/
docker-compose up -d
```
## Running on
``` shell
http://localhost:8000/
```

```shell
docker-compose exec go go run .
```

## Manage Redis Cache
```shell
docker-compose exec redis redis-cli
```

## Manage Mysql Database
```shell
docker-compose exec mysql sh 
mysql -u user -p
Enter password: user
```