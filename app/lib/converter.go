package lib

import (
	"fmt"
	"encoding/json"
	"time"

	"strconv"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
)


func HashPassword(password string) string {
    bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
    return string(bytes)
}

func CheckPasswordHash(password, hash string) bool {
    err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
    return err == nil
}

func ConvertToMD5(value string) string{
	var str string = value
	hasher := md5.New()
	hasher.Write([]byte(str))
	converId := hex.EncodeToString(hasher.Sum(nil))

	return converId
}

func ConvertToSHA1(value string) string{
    sha := sha1.New()
    sha.Write([]byte(value))
    encrypted       := sha.Sum(nil)
    encryptedString := fmt.Sprintf("%x", encrypted)
	return encryptedString
}

func ConvertToSHA256(value string) string{
	hash := sha256.Sum256([]byte(value))
	res  := fmt.Sprintf("%x", hash)
	return res
}

func ConvertStringToInt(value string) int{
	value_int, _  	:= strconv.Atoi(value)
	return value_int
}

func ConvertStringToFloat(value string) float64{
	value_float, _ 	:= strconv.ParseFloat(value, 8)
	return value_float
}

func ConvertFloatToString(input_num float64) string {
    return strconv.FormatFloat(input_num, 'f', 6, 64)
}

func ConvertJsonToString(payload interface{}) string{
	jsonData, err := json.Marshal(payload)
	if err != nil {
		logs.Println(err)
	}
	return string(jsonData)
}

func ConvertStringToJson(value string) (interface{}){
	var output interface{}
	err := json.Unmarshal([]byte(value), &output)
	if nil != err {
		// logs.Println(err)
	}
	return output
}

func ConvertStringToTime(value string) (*time.Time){
	layout := "2021-05-19 11:56:30"
	t, err := time.Parse(layout , value)
	if nil != err {
		logs.Println(err)
	}
	return &t
}
