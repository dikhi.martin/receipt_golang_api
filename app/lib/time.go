
package lib

import (
	"time"
)

func CurrentTime(format string) string{
	current_time := time.Now().Format(format)
	return current_time
}

func TimeNow() *time.Time{
	t := time.Now()
	return &t
}

func CountRangeDate(date_1 , date_2, types string) float64{
	// date_1 > date_2
	// format_date : 2006-01-02 15:04:05
	var result float64

	format     := "2006-01-02 15:04:05"
	date_one,_ := time.Parse(format, date_1)
	date_two,_ := time.Parse(format, date_2)
	
	diff 	 := date_one.Sub(date_two)

	// number of Hours
	if types == "hours"{
		result = diff.Hours()

	// number of Nanoseconds
	}else if types == "nanoseconds"{
		result = float64(diff.Nanoseconds())

	// number of Minutes
	}else if types == "minutes"{
		result = diff.Minutes()

	// number of Seconds
	}else if types == "seconds"{
		result = diff.Seconds()

	// number of Days
	}else if types == "days"{
		result = float64(diff.Hours()/24)
	}
	
	return result
}