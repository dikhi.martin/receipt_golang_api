package lib

import(
  "net/http"
  "encoding/json"
  "github.com/labstack/echo"
)


type ResponseJWT struct {
  User struct {
    ID       string `json:"id"`
    Username string `json:"username"`
    Email    string `json:"email"`
  } `json:"user"`
  AuthToken string `json:"auth_token"`
}

type ResponseError struct {
  Description string `json:"description"`
  Status      struct {
    Code    int    `json:"code"`
    Message string `json:"message"`
  } `json:"status"`
}

func DataAuth(auth_token string) (ResponseJWT, error, int){
  var result ResponseJWT
  var error  ResponseError

  url    := GetEnv("HOST_JWT_AUTH") + "/auth_data/"
  method := "GET"

  client   := &http.Client {}

  req, err := http.NewRequest(method, url, nil)
  if err != nil {
    logs.Println(err)
    return result, err, 500
  }

  req.Header.Add("Authorization", auth_token)  
  res, err := client.Do(req)
  if err != nil{
    return result, err, 500
  }
  defer res.Body.Close()

  var json_data map[string]interface{}
  json.NewDecoder(res.Body).Decode(&json_data)
  json_byte      := []byte(string(ConvertJsonToString(json_data)))

  err = json.Unmarshal(json_byte, &result)
  if err != nil {
      logs.Println(err)
      return result, err, 500
  }

  // resp_session_and_token_expire
  err = json.Unmarshal(json_byte, &error)
  if error.Status.Code != 0{
    logs.Println(error.Status.Message)
    return result, nil, error.Status.Code
  }

  return result, nil, 200
}

func CheckAuth(c echo.Context, code int, err error) bool{
  if code != 200{
    if code == 401{
      Session_expired(c)
      return false
    }
    if code == 419{
      Token_invalid(c)
      return false
    }
    if err != nil{
      logs.Println(err)
      Internal_server_error(c)
      return false
    } 
  }
  return true  
}