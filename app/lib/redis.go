package lib

import (
	"strconv"
	"fmt"
	"github.com/go-redis/redis"
)

func RedisConnection() *redis.Client {
	redisHost  := GetEnv("REDIS_HOST")
	redisPort  := GetEnv("REDIS_PORT")
	redisDB, _ := strconv.Atoi(GetEnv("REDIS_DB"))

	client := redis.NewClient(&redis.Options{
		Addr 		: ""+redisHost+":"+redisPort+"",
		Password 	: "",
		DB 			: redisDB,
	})

	pong, err := client.Ping().Result()
	if err != nil {
		fmt.Println(err)
		logs.Println(err)
	}
	fmt.Println(pong)

	return client
}

func GetTokenRedis(key string)string{
	redis_token, err := RedisConnection().Get(key).Result()
	if err != nil {
		logs.Println(err)
	}
	return redis_token
}