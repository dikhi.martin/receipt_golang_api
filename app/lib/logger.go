package lib

import (
	"log"
	"os"
	"sync"
	"time"
)

var logs = RecordLog("SYSTEMS -")

type logger struct {
	filename string
	*log.Logger
}

var logg *logger
var once sync.Once
var user string

func SetUser(username string) string {
	user = username
	return user
}

func GetUser() string {
	return user
}

func RecordLog(userlog string) *logger {
	SetUser(userlog)
	once.Do(func() {
		if GetEnv("APP_TYPE") == "production" || GetEnv("APP_TYPE") == "stagging"{
			logg = createLogger("/receipt_api.log", GetUser())
		}else{
			logg = createLogger("logs/receipt_api.log", GetUser())
		}
	})
	return logg
}

func createLogger(fname string, user string) *logger {
	file, err := os.OpenFile(fname, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil{
		log.Println("error create log :", err)
	}
	location, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		return &logger{
			filename: fname,
			Logger:   log.New(file, time.Now().Format(time.RFC3339)+" "+GetUser()+" ", log.Lshortfile),
		}
	}
	return &logger{
		filename: fname,
		Logger:   log.New(file, time.Now().In(location).Format(time.RFC3339)+" "+GetUser()+" ", log.Lshortfile),
	}
}

func Debug(cmd string) bool {
	RecordLog(GetUser()).Println(cmd)
	return true
}
