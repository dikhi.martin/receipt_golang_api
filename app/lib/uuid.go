package lib


import (
	"math/rand"
	"time"

	"github.com/chilts/sid"
	guuid "github.com/google/uuid"
	"github.com/kjk/betterguid"
	"github.com/lithammer/shortuuid"
	"github.com/oklog/ulid"
	"github.com/rs/xid"
	"github.com/segmentio/ksuid"
	"github.com/sony/sonyflake"
	// "github.com/satori/go.uuid"
)

func GenUUIDString() string{
	id := guuid.New().String()
	return id
}

func StringToUUID(s string) *guuid.UUID{
	 res, _ := guuid.Parse(s)
	 return &res
}

func GenShortUUID() string{
	id    := shortuuid.New()
	return id
}

func GenUUID() *guuid.UUID{
	id := guuid.New()
	return &id
}

func genXid() string{
	id := xid.New()
	return id.String()
}

func genKsuid() string{
	id := ksuid.New()
	return id.String()
}

func genBetterGUID() string {
	id := betterguid.New()
	return id
}

func genUlid() string{
	t 		:= time.Now().UTC()
	entropy := rand.New(rand.NewSource(t.UnixNano()))
	id 		:= ulid.MustNew(ulid.Timestamp(t), entropy)
	return id.String()
}

func genSonyflake() uint64{
	flake := sonyflake.NewSonyflake(sonyflake.Settings{})
	id, err := flake.NextID()
	if err != nil {
		logs.Println("flake.NextID() failed with %s\n", err)
	}
	// Note: this is base16, could shorten by encoding as base62 string
	return id
}

func genSid() string {
	id := sid.Id()
	return id
}