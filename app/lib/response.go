package lib

import (
	"github.com/labstack/echo"
)

// ## Define Type Global
type response_json map[string]interface{}

// reponses Status server
var Status_201  = response_json{
	"code"		: 201,
	"message"	: "data_created",
}
var Status_204 = response_json{
	"code"		: 204,
	"message"	: "no_content_found",
}
var Status_200 = response_json{
	"code"		: 200,
	"message"	: "OK",
}
var Status_304  = response_json{
	"code"		: 304,
	"message"	: "not_modified",
}
var Status_400 = response_json{
	"code"		: 400,
	"message"	: "bad_request",
}
var Status_401 = response_json{
	"code"		: 401,
	"message"	: "unauthorized",
}
var Status_403 = response_json{
	"code"		: 403,
	"message"	: "forbiden",
}
var Status_404 = response_json{
	"code"		: 404,
	"message"	: "server_not_found",
}
var Status_419 = response_json{
	"code"		: 419,
	"message"	: "token_invalid",
}
var Status_500 = response_json{
	"code"		: 500,
	"message"	: "internal_server_error",
}
var Status_503 = response_json{
	"code"		: 503,
	"message"	: "service_unavailable",
}
func Internal_server_error(c echo.Context) error{
	response := response_json{
		"status"		: Status_500,
	}
	return c.JSON(500, response)
}

func Bad_request(c echo.Context) error{
	response := response_json{
		"status"		: Status_400,
	}
	return c.JSON(400, response)
}

func Service_unavailable(c echo.Context) error{
	response := response_json{
		"status"		: Status_503,
	}
	return c.JSON(503, response)
}
func No_content_found(c echo.Context) error{
	response := response_json{
		"status"		: Status_204,
	}
	return c.JSON(200, response)
}
func Server_not_found(c echo.Context) error{
	response := response_json{
		"status"		: Status_404,
	}
	return c.JSON(404, response)
}

func Not_modified(c echo.Context) error{
	response := response_json{
		"status"		: Status_304,
	}
	return c.JSON(200, response)
}

func Token_invalid(c echo.Context) error{
	response := response_json{
		"status"		: Status_419,
		"description"	: "token_invalid",
	}
	return c.JSON(419, response)
}

func Session_expired(c echo.Context) error{
	response := response_json{
		"status"		: Status_401,
		"description"	: "session_expired",
	}
	return c.JSON(401, response)
}
func Key_redis_not_found(c echo.Context) error{
	response := response_json{
		"status"		: Status_401,
		"description"	: "key_redis_not_found",
	}
	return c.JSON(401, response)
}

func Form_required(c echo.Context, field, id_field string) error{
	response := response_json{
		"id_field"	    : id_field,
		"field"	        : field,
		"status"		: Status_400,
		"description"	: field + "_description",
	}
	return c.JSON(400, response)
}