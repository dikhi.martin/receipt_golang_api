package lib

import(
  "math/rand"
  "time"
)

// RandomString
var  seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
var  Charset         string
var  Numberset       string

func init(){ 
  Charset               =  "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  Numberset             =  "0123456789"
}

func StringWithCharset(length int, charset string) string {
  b := make([]byte, length)
  for i := range b {
    b[i] = Charset[seededRand.Intn(len(Charset))]
  }
  return string(b)
}

func StringWithNumberSet(length int, Numberset string) string {
  b := make([]byte, length)
  for i := range b {
    b[i] = Numberset[seededRand.Intn(len(Numberset))]
  }
  return string(b)
}

func RandomString(length int) string {
  return StringWithCharset(length, Charset)
}