package main
/*
 * Recipt API GORM
 *
 * API version: 1.0.0
 * Contact     : dikhi.martin@gmail.com
 */
 
import (
	"api/routes"
	"api/lib"
)

var logs 		= lib.RecordLog("SYSTEMS -")

func main() {
	e := routes.Index()
	logs.Println("Starting Application "+ lib.GetEnv("APP_NAME"))

	e.Logger.Fatal(e.Start(":"+ lib.GetEnv("APP_PORT")))
}
