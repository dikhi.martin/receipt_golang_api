package controllers

import (
    "api/lib"
)

// ## Define Config Variable Global
var logs 		  			= lib.RecordLog("SYSTEMS -")
var redis_connect 			= lib.RedisConnection()

// ## Define Type Global
type response_json map[string]interface{}
