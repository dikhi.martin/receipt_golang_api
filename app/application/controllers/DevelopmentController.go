package controllers

import (
    "fmt"
    "os"
    "bufio"
    "api/lib"  
    "api/database"  
    conf "api/config"  
	  "github.com/labstack/echo"
)

func MainPage(c echo.Context) error{
  db := database.CreateCon()
  defer db.Close()

  response  := "Name        : "+ lib.GetEnv("APP_NAME") +" \n"
  response  += "service     : High performance, minimalist Go web framework running \n"
  response  += "App type    : "+ lib.GetEnv("APP_TYPE") +" \n"

  if lib.GetEnv("APP_TYPE") == "production" || lib.GetEnv("APP_TYPE") == "stagging"{
  response  += "Last Build  : "+ conf.LastBuild +" \n"
  }else{
  response  += "Last Build  : "+ lib.CurrentTime("2006-01-02 15:04:05") +" \n"
  }

  return c.String(200, response)
}

func GetLogsDevelopmentController(c echo.Context) error {
    token_key  := c.Request().Header.Get(conf.HeaderTokenKey)
    if token_key != conf.ValueTokenKey{
      lib.Token_invalid(c)
      return nil
    }

    read_file_logs := ""
    if lib.GetEnv("APP_TYPE") == "production" || lib.GetEnv("APP_TYPE") == "stagging"{
      file, err := os.Open("poskita_mobile.log")
      if err != nil {
          logs.Println(err)
          fmt.Println(err)
          lib.Internal_server_error(c)
          return nil
      }
      defer file.Close()
      scanner      := bufio.NewScanner(file)
      for scanner.Scan() {                    // internally, it advances token based on sperator
          read_file_logs += scanner.Text()    // token in unicode-char
          read_file_logs += "\n"
      }
    }else{
      file, err := os.Open("logs/poskita_mobile.log")
      if err != nil {
          logs.Println(err)
          fmt.Println(err)
          lib.Internal_server_error(c)
          return nil
      }
      defer file.Close()
      scanner      := bufio.NewScanner(file)
      for scanner.Scan() {                    // internally, it advances token based on sperator
          read_file_logs += scanner.Text()    // token in unicode-char
          read_file_logs += "\n"
      }
    }
    return c.String(200, read_file_logs)
}

