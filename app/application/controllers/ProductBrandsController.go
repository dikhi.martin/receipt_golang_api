package controllers

import (
	     "api/application/models"
	     "api/database"	
	     "api/lib"	
	conf "api/config"	
	     "github.com/labstack/echo"
)

// == API Controller
func GetProductBrandsController(c echo.Context) error {
	db := database.CreateCon()
	defer db.Close()

	// check_token
	token_key       := c.Request().Header.Get(conf.HeaderTokenKey)
	if token_key != conf.ValueTokenKey{
		lib.Token_invalid(c)
		return nil
	}

	// == filters
	pageFilters   := c.FormValue("filters")
	pageSearch    := c.FormValue("search")
	columnFilter  := c.FormValue("column")
	Sort  		  := c.FormValue("order_by")

	pageNumber := lib.ConvertStringToInt(c.FormValue("page"))
	pageSize   := lib.ConvertStringToInt(c.FormValue("size"))
	if (pageNumber == 0){
		pageNumber = 1
	}

	// == fetch_data
	result 			:= GetDataProductBrands(pageFilters, pageSearch, columnFilter, Sort, pageNumber, pageSize)
	total_records   := lib.CountRecordsDataByTable("", pageFilters, pageSearch, columnFilter, "tb_product_brands")
	pagination 	    := lib.GeneratePagination(pageNumber, pageSize, total_records, c.QueryString(), result)

	response := response_json{
		"data"		: pagination,
		"status" 	: lib.Status_200,
	}

	return c.JSON(200, response)
}

func DetailProductBrandsController(c echo.Context) error{
	db := database.CreateCon()
	defer db.Close()

	token_key       := c.Request().Header.Get(conf.HeaderTokenKey)
	if token_key != conf.ValueTokenKey{
		lib.Token_invalid(c)
		return nil
	}

	request_id := c.Param("id")

	// == fetch_data
	data := GetDataRowProductBrands(`["id","=","`+ request_id +`"]`, "", "")
	if data.ID == ""{
		lib.No_content_found(c)
		return nil
	}	
	
	response := response_json{
		"status" 		: lib.Status_200,
		"data"    		: data,
	}
	return c.JSON(200, response)
}

func CreateProductBrandsController(c echo.Context) error{
	db := database.CreateCon()
	defer db.Close()

	token_key       := c.Request().Header.Get(conf.HeaderTokenKey)
	if token_key != conf.ValueTokenKey{
		lib.Token_invalid(c)
		return nil
	}

	data := []models.ModelsProductBrands{}
	if err := c.Bind(&data); err != nil {
		logs.Println(err)
		lib.Bad_request(c)
		return nil
	}

	result := []models.ProductBrands{}
    for _, value := range data{
    	name_brands     	:= value.Name_brands
    	slug           		:= value.Slug
    	status 	    	    := value.Status

    	// validation
		if name_brands == ""{
			lib.Form_required(c, "name_brands", "validation_name_brands")
			return nil
		}
		if slug == ""{
			slug = lib.MakeSlug(name_brands)
		}

		insert := models.ProductBrands{
			BasicModel	  : models.BasicModel{
				ID 		  : lib.GenUUID(),
				CreatedAt : lib.TimeNow(),
			},
			Name_brands 			:  name_brands,		
			Slug 		  			:  slug,		
			Status 		  			:  status,
		}
		if error_insert := db.Create(&insert); error_insert.Error != nil {
			logs.Println(error_insert)
			lib.Internal_server_error(c)
			return nil
		}
		db.NewRecord(insert)

		result 	  = append(result, insert)
    }


	response := response_json{
		"status" : lib.Status_201,
		"result" : result,
	}
	return c.JSON(201, response)
}

func UpdateProductBrandsController(c echo.Context) error{
	db := database.CreateCon()
	defer db.Close()

	token_key       := c.Request().Header.Get(conf.HeaderTokenKey)
	if token_key != conf.ValueTokenKey{
		lib.Token_invalid(c)
		return nil
	}

	data := []models.ModelsProductBrands{}
	if err := c.Bind(&data); err != nil {
		logs.Println(err)
		lib.Bad_request(c)
		return nil
	}


	result := []models.ProductBrands{}
    for _, value := range data{
    	id     			    := value.ID
    	name_brands     	:= value.Name_brands
    	slug           		:= value.Slug
    	status 	    	    := value.Status

    	// validation
		if name_brands == ""{
			lib.Form_required(c, "name_brands", "validation_name_brands")
			return nil
		}
		if slug == ""{
			slug = lib.MakeSlug(name_brands)
		}

		var update models.ProductBrands
		data := db.Model(&update).Where("id = ?", id).Updates(map[string]interface{}{
			"name_brands"     : name_brands,
			"slug" 		 		: slug,
			"status" 	 		: status,
		})
		if data.Error != nil{
			logs.Println(data.Error)
			lib.Internal_server_error(c)
			return nil
		}
		if data.RowsAffected == 0{
			lib.Not_modified(c)
			return nil
		}
		result 	  = append(result, update)
    }	

	response := response_json{
		"status" : lib.Status_200,
		"data" 	 : data,
	}
	return c.JSON(200, response)
}

func DeleteProductBrandsController(c echo.Context) error{
	db := database.CreateCon()
	defer db.Close()
	
	token_key       := c.Request().Header.Get(conf.HeaderTokenKey)
	if token_key != conf.ValueTokenKey{
		lib.Token_invalid(c)
		return nil
	}

	data := []models.ModelsProductBrands{}
	if err := c.Bind(&data); err != nil {
		logs.Println(err)
		lib.Bad_request(c)
		return nil
	}


	result := []models.ProductBrands{}
    for _, value := range data{
    	id     			    := value.ID

		var delete models.ProductBrands
		data := db.Unscoped().Where("id = ?", id).Delete(&delete)
		if data.Error != nil{
			logs.Println(data.Error)
			lib.Internal_server_error(c)
			return nil
		}
		result 	  = append(result, delete)
    }	

	response := response_json{
		"status" : lib.Status_200,
		"data" 	 : data,
	}
	return c.JSON(200, response)
}

// == Custom Function
func GetDataProductBrands(pageFilters, pageSearch , columnFilter, Sort string, pageNumber, pageSize int) ([]models.ModelsProductBrands){
	db := database.CreateCon()
	defer db.Close()

	// pagination
	pn := pageNumber - 1
	ps := pageSize
	offset := 0
	if pn >= 0 {
		offset = ps * pn
	}

	var lim interface{}
	if ps == 0{
		lim = nil
	}else{
		lim = ps
	}

	selected 				 		  							 := "id, created_at, updated_at, name_brands, slug, status, created_by, updated_by"
	queryFilter, whereFilters, querySearch, whereSearch, orderBy := lib.CreateCustomFilters(pageFilters, pageSearch, columnFilter, Sort)

	rows, err := db.
		Model(&models.ProductBrands{}).
		Select(selected).
		Where(queryFilter, whereFilters ...).
		Where(querySearch, whereSearch ...).
		Limit(lim).
		Offset(offset).	
		Order(orderBy).
		Rows()
	if err != nil {
		logs.Println(err)
	}
	defer rows.Close()

	each   := models.ModelsProductBrands{}
	result := []models.ModelsProductBrands{}

	for rows.Next() {
		var id, created_at, updated_at, name_brands, slug, status, created_by, updated_by []byte
		err = rows.Scan(&id, &created_at, &updated_at, &name_brands, &slug, &status, &created_by, &updated_by)
		if err != nil {
			logs.Println(err)
		}

		each.ID 	           = string(id)
		each.CreatedAt 	       = string(created_at)
		each.UpdatedAt 	       = string(updated_at)
		each.Name_brands 	   = string(name_brands)
		each.Slug 	   		   = string(slug)
		each.Status 	   	   = string(status)

		result 	  = append(result, each)
	}

	return result
}

func GetDataRowProductBrands(pageFilters, pageSearch , columnFilter string) (models.ModelsProductBrands){
	db := database.CreateCon()
	defer db.Close()

	selected 				 		  					   := "id, created_at, updated_at, name_brands, slug, status, created_by, updated_by"
	queryFilter, whereFilters, querySearch, whereSearch, _ := lib.CreateCustomFilters(pageFilters, pageSearch, columnFilter, "")

	var id, created_at, updated_at, name_brands, slug, status, created_by, updated_by []byte
	row := db.
	Model(&models.ProductBrands{}).
	Where(queryFilter, whereFilters ...).
	Where(querySearch, whereSearch ...).
	Select(selected).
	Row() 
	row.Scan(&id, &created_at, &updated_at, &name_brands, &slug, &status, &created_by, &updated_by)

	data := models.ModelsProductBrands{
		ID 	           	   : string(id),
		CreatedAt 	       : string(created_at),
		UpdatedAt 	       : string(updated_at),
		Name_brands 	   : string(name_brands),
		Slug 	   		   : string(slug),
		Status 	   	   	   : string(status),
	}

	return data
}