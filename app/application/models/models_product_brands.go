package models

import   (
  "github.com/jinzhu/gorm"
)

// its use for definition database GORM
type ProductBrands struct {
  BasicModel
  Name_brands              string    `gorm:"type:varchar(255); NOT NULL"`
  Slug                     string    `gorm:"type:varchar(255)"` 
  Status                   string    `gorm:"type:enum('active','inactive'); default:'active'"`
  Created_by               string    `gorm:"type:char(10)"` 
  Updated_by               string    `gorm:"type:char(10)"` 
  Additional               string    `gorm:"type:varchar(255)"`
}
func (ProductBrands) TableName() string {
  return "tb_product_brands"
}

// its use for Schema Methods database GORM
func DropColumnProductBrands(db *gorm.DB){
    // db.Model(&ProductBrands{}).DropColumn("delete_at")
}

// its use for call model from controllers
type ModelsProductBrands struct {
  ID                string       `json:"id"`
  Name_brands       string       `json:"name_brands"`
  Slug              string       `json:"slug"`
  Status            string       `json:"status"`
  CreatedAt         string       `json:"created_at"`
  UpdatedAt         string       `json:"updated_at"`
}


