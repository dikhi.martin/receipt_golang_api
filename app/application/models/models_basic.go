package models

import (
	"time"
	"github.com/google/uuid"
)

type BasicModel struct {
	ID        *uuid.UUID     `json:"id,omitempty" gorm:"primaryKey;unique;type:varchar(36);not null" format:"uuid"` // model ID
	CreatedAt *time.Time     `json:"created_at,omitempty" format:"date-time"`                                       // created at automatically inserted on post
	UpdatedAt *time.Time     `json:"updated_at,omitempty" format:"date-time"`                                       // created at automatically changed on put or add on post
}

// its use for raw request
type RequestMailSend struct {
  From                  string          `json:"from"`
  To                    []string        `json:"to"`
  Subject               string          `json:"subject"`
  
  TitleText             string          `json:"title_text"`
  SubtitleText          string          `json:"subtitle_text"`
  BodyText              string          `json:"body_text"`
  ButtonText            string          `json:"button_text"`
  ButtonLink            string          `json:"button_link"`
  ButtonMethod          string          `json:"button_method"`
  FooterText            string          `json:"footer_text"`
  Show_link_additional  bool          `json:"show_link_additional"`

  LayoutColor           string          `json:"layout_color"`
  LayoutImage           string          `json:"layout_image"`

  BusinessName          string          `json:"business_name"`
  BusinessLogo          string          `json:"business_logo"`
  BusinessTagline       string          `json:"business_tagline"`
  BusinessEmail         string          `json:"business_email"`
  BusinessPhoneNumber   string          `json:"business_phone_number"`
  BusinessAddress       string          `json:"business_address"`

  SociallinkFacebook    string          `json:"sociallink_facebook"`
  SociallinkTwiter      string          `json:"sociallink_twiter"`
  SociallinkInstagram   string          `json:"sociallink_instagram"`
  SociallinkPath        string          `json:"sociallink_path"`
}