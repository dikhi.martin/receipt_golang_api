package routes

import (
	  "github.com/labstack/echo"
	c "api/application/controllers"
)

func ApiRoutes(g *echo.Group) {
	// product_brands
	g.GET("/product_brands/get_data/",    c.GetProductBrandsController)
	g.GET("/product_brands/detail/:id/",  c.DetailProductBrandsController)
	g.POST("/product_brands/create/", 	  c.CreateProductBrandsController)
	g.PUT("/product_brands/update/",      c.UpdateProductBrandsController)
	g.POST("/product_brands/delete/",     c.DeleteProductBrandsController)
}
