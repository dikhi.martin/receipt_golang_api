package routes

import (
	"api/application/controllers"
	"github.com/labstack/echo"
)

func DevelopmentRoute(g *echo.Group) {
	DEFINE_URL := "/development"
	g.POST(DEFINE_URL +"/get_logs/", controllers.GetLogsDevelopmentController)
}