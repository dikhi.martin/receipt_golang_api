package routes

import (
	"api/lib"
	"api/application/controllers"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)


func Index() *echo.Echo {
	e := echo.New()

	// Middleware
	e.Pre(middleware.AddTrailingSlash())
    e.Use(middleware.Recover())
    e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
        AllowOrigins: []string{"*"},
        AllowHeaders: []string{"*"},
        AllowMethods: []string{"*"},
    }))

	// Custom Middleware
	lib.LogMiddleware(e)
	e.Use(lib.ServerHeader)

	// Main
	e.GET("/", controllers.MainPage)
	e.GET("/api/", controllers.MainPage)

	// RoutesGroup
	ApiGroup      := e.Group("/api")

	// Development
	DevelopmentRoute(ApiGroup)
	ApiRoutes(ApiGroup)

	return e
}
