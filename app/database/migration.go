package database

import(
	"api/application/models"
)

func AutoMigrate(){
	db := CreateCon()
	db.AutoMigrate(
		&models.ProductBrands{},
	)
	defer db.Close()
}


func ViewMigrate(){
	db := CreateCon()
	defer db.Close()

	// v_get_product_brands
	if DropViewIfExist("v_get_product_brands") != false {
		if ChecktableExist("v_get_product_brands") == true {
			db.Exec("CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `v_get_product_brands`  AS  " + v_get_product_brands() + ";")
		}
	}
}

func AddForeign(){
	// AddForeignKey("tb_products", "id_brands_products", "tb_product_brands", "id", "CASCADE", "CASCADE", "categories_foreign")
}

func AddUnique(){
	// AddUniqueField(lib.GetEnv("TB_AUTH"), "email")
}

func DropForeign(){
	// code_here
}

func Dropindex(){
	// code_here
}

func AlterColumn(){
	// code_here 
}

func CreateTriggers(){
	// code_here 
}

func DropColumn(){
	db := CreateCon()
		// models.DropColumnQuotation(db)
	defer db.Close()
}

