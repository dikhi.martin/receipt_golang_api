package database

import(
	"fmt"
	"api/lib"
)

func ChecktableRecord(table_name string) bool{
	db := CreateCon()
	defer db.Close()
	var result int
	db.Table(table_name).Count(&result)
	if result == 0{
		return false
	}
	return true
}

func CheckTableSpecificRecord(table_name, name_column, record string) bool{
	db := CreateCon()
	defer db.Close()

	var id string
	row := db.
	Table(table_name).
	Where(""+ name_column +" = ?", record).
	Select("id").
	Row() 
	err := row.Scan(&id)
	if err != nil {
		return false
	}
	return true
}

func ChecktableExist(table_name string) bool{
	db := CreateCon()
	defer db.Close()

	var table_check int
	row := db.Table(table_name).Select("COUNT(*)").Row() 
	row.Scan(&table_check)

    if table_check > 0 {
        fmt.Println(lib.Warn("Table '"+table_name+"' already exists"))
        return false
    } else {
        fmt.Println(lib.Info("Created migration view ", table_name))
        return true
    }

	return true
}

func DropView(view_name string) bool{
	db := CreateCon()
	defer db.Close()

	if err := db.Exec("DROP VIEW `"+ view_name +"`;").Error; err != nil {
	  fmt.Println(err)
	  return false
	}
	return true
}

func DropindexTable(name_table, name_index string) bool{
	db := CreateCon()
	defer db.Close()

	if err := db.Exec("ALTER TABLE "+ name_table +" DROP INDEX "+ name_index +";").Error; err != nil {
	  fmt.Println(err)
	  return false
	}
    fmt.Println(lib.Warn("DROP INDEX ", name_table +" "+ name_index))
	return true
}

func DropViewIfExist(view_name string) bool{
	db := CreateCon()
	defer db.Close()

	if err := db.Exec("DROP VIEW IF EXISTS `"+ view_name +"`;").Error; err != nil {
	  fmt.Println(err)
	  return false
	}

	return true
}

func DropTriggerIfExist(name_trigger string) bool{
	db := CreateCon()
	defer db.Close()

	if err := db.Exec("DROP TRIGGER `"+ name_trigger +"`;").Error; err != nil {
	  fmt.Println(err)
	}

    fmt.Println(lib.Succ("CREATED TRIGGER ", name_trigger))

	return true
}

func DropForeignKey(name_table, name_foreign string) bool{
	db := CreateCon()
	defer db.Close()

	if err := db.Exec("ALTER TABLE "+ name_table +" DROP FOREIGN KEY "+ name_foreign +";").Error; err != nil {
	  fmt.Println(err)
	  return false
	}
    fmt.Println(lib.Warn("DROP FOREIGN KEY ", name_table +" "+ name_foreign))

	return true
}

func ModifyColumn(name_table, name_column, type_data, length, defaults string, is_unsigned, is_null bool) bool{
	db := CreateCon()
	defer db.Close()

	TYPEDATA := type_data+"("+ length +")"

	var QUERY 	   string 
	var ISNULL     string
	var ISUNSIGNED string

	if is_null == true{
		ISNULL = "NULL"
	}else{
		ISNULL = "NOT NULL"
	}

	if is_unsigned == true{
		ISUNSIGNED = "UNSIGNED"
	}

	if defaults != ""{
		QUERY = "ALTER TABLE "+ name_table +" MODIFY COLUMN "+ name_column +" "+ TYPEDATA +" "+ ISUNSIGNED +"  "+ ISNULL +" DEFAULT "+ defaults +"  ;"
	}else{
		QUERY = "ALTER TABLE "+ name_table +" MODIFY COLUMN "+ name_column +" "+ TYPEDATA +" "+ ISUNSIGNED +"  "+ ISNULL +" ;"
	}

	if err := db.Exec(QUERY).Error; err != nil {
	  fmt.Println(err)
	  return false
	}
	
    fmt.Println(lib.Warn(QUERY))

	return true
}

func AddForeignKey(table, table_field, references, ref_field, on_delete, on_update, constraint string) bool{
	db := CreateCon()
	defer db.Close()

	QUERY := "ALTER TABLE "+ table +" ADD CONSTRAINT "+ constraint +" FOREIGN KEY ("+ table_field +") REFERENCES "+ references +" ("+ ref_field +") ON DELETE "+ on_delete +" ON UPDATE "+ on_update +";"
	if err := db.Exec(QUERY).Error; err != nil {
	  fmt.Println(err)
	  return false
	}
    fmt.Println(lib.Prim(QUERY))

	return true
}


func AddUniqueField(table, table_field string) bool{
	db := CreateCon()
	defer db.Close()

	// drop_if_exist
	DropindexTable(table, table_field)

	QUERY := "ALTER TABLE "+ table +" ADD UNIQUE ("+ table_field +") ;"
	if err := db.Exec(QUERY).Error; err != nil {
		fmt.Println(err)
	  return false
	}
    fmt.Println(lib.Prim(QUERY))

	return true
}