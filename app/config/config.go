package config

var LastBuild              		 string
var HeaderAuthToken              string
var HeaderTokenKey               string
var HeaderSecureKey              string
var HeaderTypeAccount            string
var ValueTokenKey                string
var DeepLinkCheckout             string

func init(){ 
	LastBuild     				 = "2021-10-07 14:20:49 "  	// YYYY-MM-DD HH:MM:SS

    HeaderAuthToken    			 = "Authorization" 
    HeaderTokenKey     			 = "token-key" 
    HeaderSecureKey    			 = "secure-key" 
    HeaderTypeAccount  			 = "type-account" 
    ValueTokenKey                = "085B7120909C0DB4FBC3C9C568975E6F" 
}

