module api

go 1.15

require (
	github.com/chilts/sid v0.0.0-20190607042430-660e94789ec9
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/google/uuid v1.2.0
	github.com/iancoleman/strcase v0.1.3
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/kjk/betterguid v0.0.0-20170621091430-c442874ba63a
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/oklog/ulid v1.3.1
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be
	github.com/rs/xid v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/segmentio/ksuid v1.0.3
	github.com/sony/sonyflake v1.0.0
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
